import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { DropdownDirective } from './dropdown.directive';

@NgModule({
	declarations:[
		DropdownDirective
	],
	exports:[
		DropdownDirective,
		CommonModule
	],
	imports:[
		// CommonModule, // should this be in exports? well it is in his videos.
	]
})
export class SharedModule {}